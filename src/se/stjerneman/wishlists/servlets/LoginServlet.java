package se.stjerneman.wishlists.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.stjerneman.wishlists.domain.User;
import se.stjerneman.wishlists.domain.WishListService;

public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static Logger log = Logger.getLogger(LoginServlet.class);

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("Entering LoginServlet (GET)");

        if (request.getSession().getAttribute("uid") != null) {
            response.sendRedirect(response.encodeRedirectURL(request
                    .getContextPath() + "/dashboard"));
            return;
        }

        request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request,
                response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("Entering LoginServlet (POST)");

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        ArrayList<String> errors = new ArrayList<String>();

        // Validate username.
        if (username.isEmpty()) {
            errors.add("Username can't be empty.");
        }

        // Validate password.
        if (password.isEmpty()) {
            errors.add("Password can't be empty.");
        }

        // Get the user by the username.
        User user = WishListService.getInstance().getUserByUsername(username);

        // Validate the passwords against each other.
        if (!username.isEmpty()
                && (user == null || !user.getPassword().equals(password))) {
            errors.add("Username and password don't match.");
        }

        if (errors.size() > 0) {
            request.setAttribute("errors", errors);
            request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request,
                    response);
            return;
        }

        // Set the UID and username to the session.
        request.getSession().setAttribute("uid", user.getUid());
        request.getSession().setAttribute("username", user.getUsername());

        log.debug("User \"" + user.getUsername() + "\" logged in.");

        response.sendRedirect(response.encodeRedirectURL("dashboard"));
    }
}
