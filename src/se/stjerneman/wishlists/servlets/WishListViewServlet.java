package se.stjerneman.wishlists.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.stjerneman.wishlists.domain.WishList;
import se.stjerneman.wishlists.domain.WishListService;

public class WishListViewServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private Logger log = Logger.getLogger(this.getClass().getName());

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("In WishListViewServlet (GET).");

        if (request.getSession().getAttribute("uid") == null) {
            response.sendRedirect(response.encodeRedirectURL(request
                    .getContextPath() + "/index"));
            return;
        }

        String listId = request.getParameter("listId");
        log.debug("List ID : " + listId);

        WishList list = WishListService.getInstance().getWishList(listId);

        if (list == null) {
            response.sendRedirect(response.encodeRedirectURL(""));
            return;
        }

        request.setAttribute("list", list);
        request.setAttribute("products", list.getProducts().values());

        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/WEB-INF/wishlist.jsp");
        dispatcher.forward(request, response);
    }
}
