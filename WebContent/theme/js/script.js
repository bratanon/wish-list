(function($) {
  $(document).ready(function() {
    $("html").addClass("js");

    $(".hide-labels input[type='text'], .hide-labels input[type='password']").each(function() {
      var $labelField = $("[for='" + $(this).attr("id") + "']");
      $labelField.show();

      var left = ($labelField.outerWidth() - 4) + "px";

      $(this).focus(function() {
        $labelField.animate({
          left : "-" + left
        }, {
          queue : false
        });
      }).blur(function() {
        $labelField.animate({
          left : 2
        }, {
          queue : false
        });
      });
    });

    $("a.not-implemented").click(function(event) {
      event.preventDefault();

      $("#not-implemented-popup").dialog({
        modal : true,
        draggable : false,
        resizable : false,
        width : "auto",
        maxWidth : "100%",
        minHeight : 10,
        fluid : true
      });

      $("#not-implemented-popup a").click(function(event) {
        event.preventDefault();

        $("#not-implemented-popup").dialog("close");
      });
    });

    // on window resize run function
    $(window).resize(function() {
      fluidDialog();
    });

    // catch dialog if opened within a viewport smaller than the dialog
    // width
    $(document).on("dialogopen", ".ui-dialog", function(event, ui) {
      fluidDialog();
    });

    function fluidDialog() {
      var $visible = $(".ui-dialog:visible");
      // each open dialog
      $visible.each(function() {
        var $this = $(this);
        var dialog = $this.find(".ui-dialog-content").data(
            "ui-dialog");
        // if fluid option == true
        if (dialog.options.fluid) {
          var wWidth = $(window).width();
          // check window width against dialog width
          if (wWidth < dialog.options.maxWidth + 50) {
            // keep dialog from filling entire screen
            $this.css("max-width", "90%");
          }
          else {
            // fix maxWidth bug
            $this.css("max-width", dialog.options.maxWidth);
          }
          // reposition dialog
          dialog.option("position", dialog.options.position);
        }
      });
    }
  });
})(jQuery);
