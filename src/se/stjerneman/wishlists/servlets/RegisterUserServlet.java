package se.stjerneman.wishlists.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.stjerneman.wishlists.domain.User;
import se.stjerneman.wishlists.domain.WishListService;

public class RegisterUserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static Logger log = Logger.getLogger(RegisterUserServlet.class);

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("Entering RegisterUserServlet (GET).");

        if (request.getSession().getAttribute("uid") != null) {
            response.sendRedirect(response.encodeRedirectURL(request
                    .getContextPath() + "/dashboard"));
            return;
        }

        request.getRequestDispatcher("/WEB-INF/register.jsp").forward(request,
                response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("Entering RegisterUserServlet (POST).");

        String username = request.getParameter("username").trim();
        String password = request.getParameter("password").trim();

        ArrayList<String> errors = new ArrayList<String>();

        // Validate username.
        if (username.isEmpty()) {
            errors.add("Username can't be empty.");
        }

        // Validate password.
        if (password.isEmpty()) {
            errors.add("Password can't be empty.");
        }

        // Validate that the username isn't already taken.
        if (!username.isEmpty()
                && (WishListService.getInstance().getUserByUsername(username) != null)) {
            errors.add("Sorry, that username is already taken.");
        }

        if (errors.size() > 0) {
            request.setAttribute("errors", errors);
            request.getRequestDispatcher("/WEB-INF/register.jsp").forward(
                    request, response);
            return;
        }

        log.debug("Everything went fint, lets create the user.");

        // Create a new user, and add to the service.
        User user = new User(username, password);
        WishListService.getInstance().addUser(user);

        // Set the UID and username to the session.
        request.getSession().setAttribute("uid", user.getUid());
        request.getSession().setAttribute("username", user.getUsername());

        response.sendRedirect(response.encodeRedirectURL("dashboard"));
    }
}
