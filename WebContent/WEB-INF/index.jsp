<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Wishlister</title>
	<%@ include file="styles.jsp" %>
	<%@ include file="scripts.jsp" %>
</head>
<body>
	<%@ include file="header.jsp" %>
	<div id="container">
		<section id="services" class="clearfix">
	    <h2 class="pane-title">WishLister - Don't you wish for something?</h2>
	    <div>
	      <div>
	        <img src="${pageContext.request.contextPath}/theme/images/red.png" alt="Lorem ipsum" />
	        <h3>Lorem ipsum</h3>
	        <p>Morbi interdum dolor vitae quam suscipit varius. Maecenas eu ligula volutpat, interdum sem sed, blandit dui.</p>
	      </div>
	      <div>
	        <img src="${pageContext.request.contextPath}/theme/images/blue.png" alt="Lorem ipsum" />
	        <h3>Lorem ipsum</h3>
	        <p>Morbi interdum dolor vitae quam suscipit varius. Maecenas eu ligula volutpat, interdum sem sed, blandit dui.</p>
	      </div>
	      <div>
	        <img src="${pageContext.request.contextPath}/theme/images/yellow.png" alt="Lorem ipsum" />
	        <h3>Lorem ipsum</h3>
	        <p>Morbi interdum dolor vitae quam suscipit varius. Maecenas eu ligula volutpat, interdum sem sed, blandit dui.</p>
	      </div>
	      <div>
	        <img src="${pageContext.request.contextPath}/theme/images/green.png" alt="Lorem ipsum" />
	        <h3>Lorem ipsum</h3>
	        <p>Morbi interdum dolor vitae quam suscipit varius. Maecenas eu ligula volutpat, interdum sem sed, blandit dui.</p>
	      </div>
	    </div>
	  </section>
	  
	  <section id="register" class="clearfix">
	    <h2 class="pane-title">Sign up to create wish lists like never before.</h2>
	    <div class="col-left">
	      <p>Maecenas malesuada sapien quis sem commodo, ac sollicitudin est convallis. Proin ornare, urna at varius faucibus, massa nunc congue felis.</p>
	      <p>Eu vehicula ante nibh ut <a href="#" class="not-implemented">massa</a>. In vehicula facilisis consectetur. Nulla dapibus leo non augue pharetra, eget vulputate nunc condimentum. </p>
	      <p>Proin non rutrum dui, <strong>et dignissim risus integer eu lorem erat</strong>.</p>
	    </div>
	    <div class="col-right">
	      <%@ include file="registrationForm.jsp" %>
	      <a href="<c:url value="login" />" class="button" id="login-link">I have an account, let me login</a>
	    </div>
	  </section>
	</div>
	
	<%@ include file="footer.jsp" %>
</body>
</html>