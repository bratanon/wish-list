package se.stjerneman.wishlists.domain;

import java.util.UUID;

public class User {

    /**
     * This users universally unique identifier.
     */
    private String uid;

    /**
     * This users username.
     */
    private String username;

    /**
     * This users password.
     * 
     * This is just in plane text now, as this is just a demo site.
     */
    private String password;

    public User() {
    }

    public User(String username, String password) {
        setUid();
        setUsername(username);
        setPassword(password);
    }

    public String getUid() {
        return uid;
    }

    private void setUid() {
        this.uid = UUID.randomUUID().toString();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
