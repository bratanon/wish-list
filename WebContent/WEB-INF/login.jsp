<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="wishListTag" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Login - Wishlister</title>
	<%@ include file="styles.jsp" %>
	<%@ include file="scripts.jsp" %>
</head>
<body>
	<%@ include file="header.jsp" %>
	<div id="container">
		
		<wishListTag:error-tag errors="${errors}" />
		
		<section id="login" class="clearfix">
			<h2 class="pane-title">Sign in</h2>
			<form action="<c:url value="login" />" method="post" id="form-login" class="hide-labels">
				<div class="form-item">
					<label for="field-login-username">Username</label>
					<input type="text" id="field-login-username" name="username" placeholder="Username" />
				</div>
				
				<div class="form-item">
					<label for="field-login-password">Password</label>
					<input type="password" id="field-login-password" name="password" placeholder="Password" />
				</div>
				
				<div class="form-actions">
					<input type="submit" id="field-login-submit" name="op" value="Sign in" />
				</div>
			</form>
		</section>
	</div>
	<%@ include file="footer.jsp" %>
</body>
</html>