<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<footer class="clearfix">
  <div class="inner clearfix">
    <nav id="footer-nav">
      <ul>
        <li><a href="<c:url value="/" />">Home</a></li>
        <li><a href="#" class="not-implemented">About</a></li>
        <li><a href="#" class="not-implemented">Lists</a></li>
        <li><a href="#" class="not-implemented">Contact</a></li>
      </ul>
    </nav>
    <p>&copy; 2014 Emil Stjerneman<br />Plushögskolan GBJU13</p>
  </div>
</footer>

<div id="not-implemented-popup">
	<h2>Not implemented</h2>
	<p>The resource this link links to is not yet implemented.</p>
	<a href="#">Close</a>
</div>

