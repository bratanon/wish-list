package se.stjerneman.wishlists.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * A filter that checks if the session contains valid uid, otherwise the user is
 * redirected to the index page.
 */
public class CheckActiveSessionFilter implements Filter {

    private static Logger log = Logger
            .getLogger(CheckActiveSessionFilter.class);

    // Ignore this filter for certain URLs.
    private String ignoreUrlPattern;

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp,
            FilterChain filterChain) throws IOException, ServletException {

        log.debug("Entering CheckActiveSessionFilter (GET)");

        HttpServletRequest httpReq = (HttpServletRequest) req;

        // Check if logout parameter exists. In that case invalidate the session
        // and return to the index page.
        String logoutParam = req.getParameter("logout");
        if (logoutParam != null) {
            log.debug("Logout parameter found, redirect to index page");
            httpReq.getSession().invalidate();
            redirectToIndex(httpReq, (HttpServletResponse) resp);
            return;
        }
        req.setAttribute("uid", httpReq.getSession().getAttribute("uid"));

        filterChain.doFilter(req, resp);

    }

    private void redirectToIndex(HttpServletRequest req,
            HttpServletResponse resp) throws IOException {
        resp.sendRedirect(resp.encodeRedirectURL("index"));
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        this.ignoreUrlPattern = config.getInitParameter("ignoreUrl");
    }

    @Override
    public void destroy() {
    }
}
