package se.stjerneman.wishlists.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.stjerneman.wishlists.domain.User;
import se.stjerneman.wishlists.domain.WishList;
import se.stjerneman.wishlists.domain.WishListService;

public class WishListServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static Logger log = Logger.getLogger(WishListServlet.class);

    protected String operation = null;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        this.operation = servletConfig.getInitParameter("operation");
    }

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("Entering WishListServlet (GET).");

        if (request.getSession().getAttribute("uid") == null) {
            response.sendRedirect(response.encodeRedirectURL(request
                    .getContextPath() + "/index"));
            return;
        }

        switch (operation) {
            case "add":
                setAddAttributes(request);
                request.getRequestDispatcher("/WEB-INF/addWishlist.jsp")
                        .forward(request, response);
                break;
            case "delete":
                deleteWishList(request, response);
                break;
            case "edit":
                String listId = request.getParameter("listId");
                log.debug("List ID to edit : " + listId);

                WishList list = WishListService.getInstance().getWishList(
                        listId);
                if (list == null) {
                    response.sendRedirect(response
                            .encodeRedirectURL("../dashboard"));
                    return;
                }

                // Validate that the user have access to this list.
                if (!list
                        .getOwner()
                        .getUid()
                        .equals(request.getSession().getAttribute("uid")
                                .toString())) {
                    response.sendRedirect(response
                            .encodeRedirectURL("../dashboard"));
                    return;
                }

                setEditAttributes(request);
                request.setAttribute("list", list);
                request.getRequestDispatcher("/WEB-INF/addWishlist.jsp")
                        .forward(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("Entering WishListServlet (POST).");
        log.debug("Operation: " + operation);

        if (request.getSession().getAttribute("uid") == null) {
            response.sendRedirect(response.encodeRedirectURL(request
                    .getContextPath() + "/index"));
            return;
        }

        switch (operation) {
            case "add":
                AddWishList(request, response);
                break;
            case "edit":
                editWishList(request, response);
                break;
        }
    }

    private void setAddAttributes(HttpServletRequest request) {
        request.setAttribute("headTitle", "Add wish list");
        request.setAttribute("pageTitle", "Create a new wish list");
        request.setAttribute("buttonValue", "Save");
        request.setAttribute("formAction", "/add/wishlist");
    }

    private void AddWishList(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        String title = request.getParameter("title");

        ArrayList<String> errors = new ArrayList<String>();

        if (title.trim().isEmpty()) {
            log.debug("Title is empty.");
            errors.add("Title can't be empty.");
        }

        if (errors.size() > 0) {
            setAddAttributes(request);
            request.setAttribute("errors", errors);
            request.getRequestDispatcher("/WEB-INF/addWishlist.jsp").forward(
                    request, response);
            return;
        }

        log.debug("Creating new wishlist with the title : " + title);

        WishListService service = WishListService.getInstance();

        User owner = service.getUser(request.getAttribute("uid").toString());
        WishList wishlist = new WishList(title, owner);

        service.addWishList(wishlist);

        response.sendRedirect(response.encodeRedirectURL("../wishlist?listId="
                + wishlist.getId()));
    }

    private void deleteWishList(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("About to delete a wish list.");

        String listId = request.getParameter("listId");
        log.debug("List ID to delete : " + listId);

        WishListService.getInstance().deleteWishList(listId);

        response.sendRedirect(response.encodeRedirectURL("../dashboard"));
    }

    private void editWishList(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        String listId = request.getParameter("listId");
        String title = request.getParameter("title");

        ArrayList<String> errors = new ArrayList<String>();

        if (title.trim().isEmpty()) {
            log.debug("Title is empty.");
            errors.add("Title can't be empty.");
        }

        if (errors.size() > 0) {
            setEditAttributes(request);
            request.setAttribute("errors", errors);
            request.getRequestDispatcher("/WEB-INF/addWishlist.jsp").forward(
                    request, response);
            return;
        }

        WishListService.getInstance().getWishList(listId).setTitle(title);
        response.sendRedirect(response.encodeRedirectURL("../dashboard"));
    }

    private void setEditAttributes(HttpServletRequest request) {
        request.setAttribute("headTitle", "Edit wish list");
        request.setAttribute("pageTitle", "Edit wish list");
        request.setAttribute("buttonValue", "Update list");
        request.setAttribute("formAction", "/edit/wishlist");
    }

}
