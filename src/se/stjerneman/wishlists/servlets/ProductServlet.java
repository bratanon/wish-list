package se.stjerneman.wishlists.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.stjerneman.wishlists.domain.Product;
import se.stjerneman.wishlists.domain.User;
import se.stjerneman.wishlists.domain.WishList;
import se.stjerneman.wishlists.domain.WishListService;

public class ProductServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static Logger log = Logger.getLogger(WishListServlet.class);

    protected String operation = null;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        this.operation = servletConfig.getInitParameter("operation");
    }

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("Entering ProductServlet (GET).");

        if (request.getSession().getAttribute("uid") == null) {
            response.sendRedirect(response.encodeRedirectURL(request
                    .getContextPath() + "/index"));
            return;
        }

        switch (operation) {
            case "add":
                showAddProductPage(request, response);
                break;
            case "edit":
                showEditProductPage(request, response);
                break;
            case "delete":
                deleteProduct(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("Entering ProductServlet (POST).");

        if (request.getSession().getAttribute("uid") == null) {
            response.sendRedirect(response.encodeRedirectURL(request
                    .getContextPath() + "/index"));
            return;
        }

        switch (operation) {
            case "add":
                addProduct(request, response);
                break;
            case "edit":
                editProduct(request, response);
                break;
        }
    }

    private void showAddProductPage(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        setAddAttributes(request);

        request.setAttribute("listId", request.getParameter("listId"));

        request.getRequestDispatcher("/WEB-INF/addProduct.jsp").forward(
                request, response);
    }

    private void addProduct(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        setAddAttributes(request);
        String listId = request.getParameter("listId");
        String name = request.getParameter("name");
        String priceStr = request.getParameter("price");
        Float price = 0f;
        String url = request.getParameter("url");

        ArrayList<String> errors = new ArrayList<String>();

        if (name.trim().isEmpty()) {
            errors.add("The product must have a name.");
        }

        try {
            if (!priceStr.trim().isEmpty()) {
                price = Float.parseFloat(priceStr);
            }
        }
        catch (NumberFormatException e) {
            errors.add("Price must be a number.");
        }

        if (errors.size() > 0) {
            log.debug("We got errors when creating a product.");
            request.setAttribute("listId", listId);
            request.setAttribute("errors", errors);
            request.getRequestDispatcher("/WEB-INF/addProduct.jsp").forward(
                    request, response);
            return;
        }
        WishListService service = WishListService.getInstance();

        User owner = service.getUser(request.getAttribute("uid").toString());
        Product product = new Product(name, owner);

        if (price > 0) {
            product.setPrice(price);
        }

        if (!url.trim().isEmpty()) {
            product.setUrl(url.trim());
        }

        // Add the product to the wishList.
        service.addProduct(listId, product);

        response.sendRedirect(response.encodeRedirectURL("../wishlist?listId="
                + listId));
    }

    private void showEditProductPage(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        setEditAttributes(request);

        String productId = request.getParameter("productId");
        String listId = request.getParameter("listId");
        log.debug("Product ID to edit : " + productId);
        log.debug("IN list : " + listId);

        WishList list = WishListService.getInstance().getWishList(listId);
        if (list == null) {
            response.sendRedirect(response.encodeRedirectURL("../dashboard"));
            return;
        }

        // Validate that the user have access to this list.
        if (!list.getOwner().getUid()
                .equals(request.getSession().getAttribute("uid").toString())) {
            response.sendRedirect(response.encodeRedirectURL("../dashboard"));
            return;
        }

        Product product = list.getProduct(productId);
        if (product == null) {
            response.sendRedirect(response.encodeRedirectURL("../dashboard"));
            return;
        }

        // Validate that the user have access to this list.
        if (!product.getOwner().getUid()
                .equals(request.getSession().getAttribute("uid").toString())) {
            response.sendRedirect(response.encodeRedirectURL("../dashboard"));
            return;
        }

        setEditAttributes(request);
        request.setAttribute("product", product);
        request.setAttribute("productId", productId);
        request.setAttribute("listId", listId);

        request.getRequestDispatcher("/WEB-INF/addProduct.jsp").forward(
                request, response);
    }

    private void editProduct(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        setEditAttributes(request);
        String productId = request.getParameter("productId");
        String listId = request.getParameter("listId");
        String name = request.getParameter("name");
        String priceStr = request.getParameter("price");
        Float price = 0f;
        String url = request.getParameter("url");

        ArrayList<String> errors = new ArrayList<String>();

        if (name.trim().isEmpty()) {
            errors.add("The product must have a name.");
        }

        try {
            if (!priceStr.trim().isEmpty()) {
                price = Float.parseFloat(priceStr);
            }
        }
        catch (NumberFormatException e) {
            errors.add("Price must be a number.");
        }

        WishListService service = WishListService.getInstance();

        Product product = WishListService.getInstance().getWishList(listId)
                .getProduct(productId);

        if (errors.size() > 0) {
            log.debug("We got errors when creating a product.");
            request.setAttribute("product", product);
            request.setAttribute("productId", productId);
            request.setAttribute("listId", listId);
            request.setAttribute("errors", errors);
            request.getRequestDispatcher("/WEB-INF/addProduct.jsp").forward(
                    request, response);
            return;
        }

        product.setName(name.trim());
        product.setPrice(price);
        product.setUrl(url.trim());

        response.sendRedirect(response.encodeRedirectURL("../wishlist?listId="
                + listId));
    }

    private void deleteProduct(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("About to delete a product.");

        String productId = request.getParameter("productId");
        String listId = request.getParameter("listId");
        log.debug("Product ID to delete : " + productId);
        log.debug("IN list : " + listId);

        WishListService.getInstance().deleteProduct(listId, productId);

        response.sendRedirect(response.encodeRedirectURL("../wishlist?listId="
                + listId));
    }

    private void setAddAttributes(HttpServletRequest request) {
        request.setAttribute("headTitle", "Add product");
        request.setAttribute("pageTitle", "Add new product");
        request.setAttribute("buttonValue", "Save");
        request.setAttribute("formAction", "/add/product");
    }

    private void setEditAttributes(HttpServletRequest request) {
        request.setAttribute("headTitle", "Edit product");
        request.setAttribute("pageTitle", "Edit product");
        request.setAttribute("buttonValue", "Update");
        request.setAttribute("formAction", "/edit/product");
    }

}
