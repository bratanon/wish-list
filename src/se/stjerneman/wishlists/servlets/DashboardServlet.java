package se.stjerneman.wishlists.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.stjerneman.wishlists.domain.WishListService;

public class DashboardServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private final Logger log = Logger.getLogger(this.getClass().getName());

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("Entering DashboardServlet (GET).");

        if (request.getSession().getAttribute("uid") == null) {
            response.sendRedirect(response.encodeRedirectURL(request
                    .getContextPath() + "/index"));
            return;
        }

        request.setAttribute("myLists", WishListService.getInstance()
                .getMyWishList((String) request.getAttribute("uid")));

        request.getRequestDispatcher("/WEB-INF/dashboard.jsp").forward(request,
                response);
    }

}
