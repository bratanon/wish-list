<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="wishListTag" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Register - Wishlister</title>
	<%@ include file="styles.jsp" %>
	<%@ include file="scripts.jsp" %>
</head>
<body class="register">
	<%@ include file="header.jsp" %>
	
	<div id="container">
	
		<wishListTag:error-tag errors="${errors}" />
			
		<section id="register" class="clearfix">
			<h2 class="pane-title">Register account</h2>
			<%@ include file="registrationForm.jsp" %>
		</section>
	</div>
	
	<%@ include file="footer.jsp" %>
</body>
</html>