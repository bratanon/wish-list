package se.stjerneman.wishlists.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import se.stjerneman.wishlists.domain.WishList;

public class WishListELFuntions {
    private static Logger log = Logger.getLogger(WishListELFuntions.class);

    /**
     * Gets the current date.
     * 
     * @param timestamp
     *            the timestamp to format.
     * @param pattern
     *            the pattern describing the date and time format
     * @return a formated string based on the timestamp and pattern.
     */
    public static String getFormatedDateFromTimestamp(long timestamp,
            String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(new Date(timestamp));
    }

    /**
     * Gets the amount of products a list has.
     * 
     * @param list
     *            the list to count products in.
     * @return the amount of products.
     */
    public static int getProductCount(WishList list) {
        log.debug("Entering getProductCount().");
        log.debug(list.getProducts().size());
        return list.getProducts().size();
    }
}
