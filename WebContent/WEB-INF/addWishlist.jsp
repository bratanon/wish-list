<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="wishListTag" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>${headTitle} - Wishlister</title>
	<%@ include file="styles.jsp" %>
	<%@ include file="scripts.jsp" %>
</head>
<body>
	<%@ include file="header.jsp" %>
	
	<div id="container">
	
		<wishListTag:error-tag errors="${errors}" />
		
		<section id="wish-list-form" class="clearfix">
			<h2 class="pane-title">${pageTitle}</h2>
		    <form action="<c:url value="${formAction}" />" method="post" class="col-40 hide-labels">
				<div class="form-item">
					<label for="field-add-wish-list-title">Title</label>
					<input type="text" id="field-add-wish-list-title" name="title" value="${list.title}" placeholder="Wish list title" />
				</div>
				<div class="form-actions">
					<input type="submit" id="field-add-wish-list-submit" name="op" value="${buttonValue}" />
				</div>
				<c:if test="${not empty list.id}">
					<input type="hidden" name="listId" value="${list.id}">
				</c:if>
			</form>
		</section>
		
		<a href="<c:url value="/dashboard" />" class="backlink">Back to dashboard.</a>
	</div>
	
	<%@ include file="footer.jsp" %>
</body>
</html>