package se.stjerneman.wishlists.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import se.stjerneman.wishlists.domain.WishListService;

public class StartupListener implements ServletContextListener {

    private static Logger log = Logger.getLogger(StartupListener.class);

    /**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        log.debug("Servlet context initialized. Starting up server and client");

        // Create demo data.
        WishListService.getInstance().createDemoData();
    }

    /**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    @Override
    public void contextDestroyed(ServletContextEvent arg0) {

    }

}
