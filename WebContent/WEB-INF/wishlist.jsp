<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>${list.title} - Wishlister</title>
	<%@ include file="styles.jsp" %>
	<%@ include file="scripts.jsp" %>
</head>
<body>
	<%@ include file="header.jsp" %>
	
	<div id="container">
		<section id="wish-list-view" class="clearfix">
			<h2 class="pane-title"><c:out value="${list.title}" /></h2>
			
			<c:if test="${empty products}">
				No products in the list.
			</c:if>
			
			<c:forEach items="${products}" var="product">
				<div class="product">
					<div class="product-actions">
						<a href="<c:url value="/edit/product?productId=${product.id}&listId=${list.id}" />">Edit</a>
						<a href="<c:url value="/delete/product?productId=${product.id}&listId=${list.id}" />">Delete</a>
					</div>
					<h3><c:out value="${product.name}" /></h3>
					<div class="metadata">
						<span>Price: <fmt:formatNumber type="number" minFractionDigits="2" value="${product.price}" /></span>
						<span><a href="<c:out value="${product.url}" />"><c:out value="${product.url}" /></a></span>
					</div>
				</div>
			</c:forEach>
		</section>
		<div class="local-task">
			<a href="<c:url value="/add/product?listId=${list.id}" />">Add product</a>
		</div>
		
		<a href="<c:url value="/dashboard" />" class="backlink">Back to dashboard</a>
		
	</div>
	
	<%@ include file="footer.jsp" %>
</body>
</html>
