<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<header>
  <div class="inner clearfix">
  	<h1><a href="<c:url value="/" />"><strong>Wish</strong>Lister</a></h1>
    <nav id="head-nav">
      <ul>
        <c:choose>
			<c:when test="${not empty sessionScope.uid}">
				<li><a href="<c:url value="/add/wishlist" />">Create new wish list</a></li>
				<li><a href="<c:url value="?logout" />">Logout</a></li>
			</c:when>
			<c:otherwise>
				<li><a href="<c:url value="/register" />">Register account</a></li>
				<li><a href="<c:url value="/login" />">Login</a></li>
			</c:otherwise>
		</c:choose>
      </ul>
    </nav>
  </div>
</header>