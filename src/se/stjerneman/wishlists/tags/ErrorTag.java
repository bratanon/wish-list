package se.stjerneman.wishlists.tags;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class ErrorTag extends SimpleTagSupport {
    private ArrayList<String> errors;

    public ArrayList<String> getErrors() {
        return errors;
    }

    public void setErrors(ArrayList<String> errors) {
        this.errors = errors;
    }

    @Override
    public void doTag() throws JspException, IOException {
        getJspContext().setAttribute("errors", errors);
        getJspBody().invoke(null);
    }
}
