package se.stjerneman.wishlists.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class IndexServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private static Logger log = Logger.getLogger(IndexServlet.class);

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("Entering IndexServlet (GET).");

        if (request.getSession().getAttribute("uid") != null) {
            response.sendRedirect(response.encodeRedirectURL(request
                    .getContextPath() + "/dashboard"));
            return;
        }

        request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request,
                response);
    }
}
