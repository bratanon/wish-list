<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="wl" uri="se.stjerneman.wishlists" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Dashboard - Wishlister</title>
	<%@ include file="styles.jsp" %>
	<%@ include file="scripts.jsp" %>
</head>
<body>
	<%@ include file="header.jsp" %>
	
	<div id="container">	
		
		<h2 class="pane-title">Hello ${sessionScope.username}</h2>
		
		<section id="" class="clearfix">
			<h2 class="pane-title">My lists</h2>
			
			<c:if test="${empty myLists}">
				You don't have any own lists yet.
			</c:if>
	
			<c:forEach items="${myLists}" var="list">
				<div class="wishlist">
					<div class="wishlist-actions">
						<a href="<c:url value="/edit/wishlist?listId=${list.id}" />">Edit</a>
						<a href="<c:url value="/delete/wishlist?listId=${list.id}" />">Delete</a>
					</div>
					<h3><a href="<c:url value="/wishlist?listId=${list.id}" />"><c:out value="${list.title}" /></a></h3>
					
					<div class="metadata">
						<span>By : <c:out value="${list.owner.username}" /></span>
						<span>Contains <strong>${wl:getProductCount(list)}</strong> products</span>
						<span>${wl:getFormatedDateFromTimestamp(list.timestamp, "Y-MM-dd HH:mm")}</span>
					</div>
				</div>
			</c:forEach>
		</section>
	</div>
	
	<%@ include file="footer.jsp" %>
</body>
</html>