<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<form action="<c:url value="register" />" method="post" id="form-register" class="hide-labels">
	<div class="form-item">
		<label for="field-register-username">Username</label>
		<input type="text" id="field-register-username" name="username" placeholder="Pick a username" />
	</div>
	
	<div class="form-item">
		<label for="field-register-password">Password</label>
		<input type="password" id="field-register-password" name="password" placeholder="Create a password" />
	</div>
	
	<div class="form-actions">
		<input type="submit" id="field-register-submit" name="op" value="Sign up for WishList" />
	</div>
</form>