package se.stjerneman.wishlists.domain;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * POJO class for wish lists.
 * 
 * @author Emil Stjerneman
 * 
 */
public class WishList {

    /**
     * Wish list universally unique identifier.
     */
    private String id;

    /**
     * Wish list title.
     */
    private String title;

    /**
     * Wish list owner.
     */
    private User owner;

    /**
     * Wish list creating time.
     */
    private long timestamp;

    /**
     * Wish list products.
     */
    private Map<String, Product> products;

    /**
     * WishList constructor.
     * 
     * @param title
     *            wish list title.
     * @param owner
     *            wish list owner.
     */
    public WishList(String title, User owner) {
        setId();
        setTimestamp();
        setTitle(title);
        setOwner(owner);
        products = new HashMap<String, Product>();
    }

    public String getId() {
        return id;
    }

    private void setId() {
        id = UUID.randomUUID().toString();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getOwner() {
        return owner;
    }

    private void setOwner(User owner) {
        this.owner = owner;
    }

    public long getTimestamp() {
        return timestamp;
    }

    private void setTimestamp() {
        Date date = new Date();
        timestamp = new Timestamp(date.getTime()).getTime();
    }

    public Map<String, Product> getProducts() {
        return products;
    }

    public Product getProduct(String productId) {
        return products.get(productId);
    }

    public void addProduct(Product product) {
        products.put(product.getId(), product);
    }

    public void deleteProduct(String productId) {
        products.remove(productId);
    }

}
