package se.stjerneman.wishlists.domain;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

/**
 * Wish list service class handles all lists and products and the connections
 * between them.
 * 
 * @author Emil Stjerneman
 * 
 */
public class WishListService {
    /**
     * Static instance of this class.
     */
    public static WishListService service;

    /**
     * Logger.
     */
    private static Logger log = Logger.getLogger(WishListService.class);

    /**
     * Map that contains all users.
     */
    private final Map<String, User> wishlist_users = new HashMap<String, User>();

    /**
     * Map that contains all lists.
     */
    private final Map<String, WishList> wishlist_lists = new TreeMap<String, WishList>();

    public static WishListService getInstance() {
        if (service == null) {
            service = new WishListService();
        }

        return service;
    }

    private WishListService() {
    }

    /**
     * Creates demo data when the server starts.
     * 
     * @see se.stjerneman.wishlister.listener.StartupListerner.java
     */
    public void createDemoData() {
        User user_admin = new User("admin", "admin");
        User user_john = new User("JohnDoe", "john");
        User user_jane = new User("JaneDoe", "jane");

        addUser(user_admin);
        addUser(user_john);
        addUser(user_jane);

        WishList list_admin = new WishList("Admins list", user_admin);
        WishList list_admin2 = new WishList("Admins list2", user_admin);
        WishList list_admin3 = new WishList("Admins list3", user_admin);
        WishList list_admin4 = new WishList("Admins list4", user_admin);
        WishList list_admin5 = new WishList("Admins list5", user_admin);
        WishList demolist1 = new WishList("Wishlist for x-mas", user_john);
        WishList demolist2 = new WishList("Birthday list", user_jane);

        Product product1 = new Product(
                "Magisk fjärrkontroll á la Harry Potter", user_admin);
        product1.setPrice(595);
        product1.setUrl("http://www.presentjakt.se/p/1314/magisk-fjarrkontroll-magic-wand");

        Product product2 = new Product("Herrklocka", user_admin);
        product2.setPrice(2500);
        product2.setUrl("http://www.presentjakt.se/p/913/personlig-herrklocka");

        Product product3 = new Product("Magnetarmband", user_admin);
        product3.setPrice(169);

        Product product4 = new Product("Stålmannen morgonrock", user_admin);

        Product product5 = new Product("T-shirt \"Alfahanne\"", user_admin);
        product5.setPrice(169);
        product5.setUrl("http://www.presentjakt.se/p/1219/alfahanne-tshirt");

        list_admin.addProduct(product1);
        list_admin.addProduct(product2);
        list_admin.addProduct(product3);
        list_admin.addProduct(product4);
        list_admin.addProduct(product5);

        wishlist_lists.put(list_admin.getId(), list_admin);
        wishlist_lists.put(list_admin2.getId(), list_admin2);
        wishlist_lists.put(list_admin3.getId(), list_admin3);
        wishlist_lists.put(list_admin4.getId(), list_admin4);
        wishlist_lists.put(list_admin5.getId(), list_admin5);

        wishlist_lists.put(demolist1.getId(), demolist1);
        wishlist_lists.put(demolist2.getId(), demolist2);

    }

    /**
     * Gets a user by its username.
     * 
     * @param username
     *            the username to get.
     * @return a a populated user object if it exists, otherwise null.
     */
    public User getUserByUsername(String username) {
        for (User user : wishlist_users.values()) {
            if (user.getUsername().toLowerCase().equals(username.toLowerCase())) {
                return user;
            }
        }

        return null;
    }

    /**
     * Gets a user by its uid.
     * 
     * @param uid
     *            the users uid.
     * @return a a populated user object if it exists, otherwise null.
     */
    public User getUser(String uid) {
        User user;
        if ((user = wishlist_users.get(uid)) != null) {
            return user;
        }
        return null;
    }

    /**
     * Adds a user to the list of users.
     * 
     * @param user
     *            the user to add.
     */
    public void addUser(User user) {
        wishlist_users.put(user.getUid(), user);
    }

    /**
     * Gets all wish lists.
     * 
     * @return a list of wish lists.
     */
    public List<WishList> getAllWishLists() {
        // Sort the list by timestamp.
        ValueComparator vc = new ValueComparator(wishlist_lists);
        return new ArrayList<WishList>(vc.base.values());
    }

    /**
     * Gets a wish list with the given id.
     * 
     * @param id
     *            wish list id to get.
     * @return a wish list or null if this map don't contains the given key
     *         (id).
     */
    public WishList getWishList(String id) {
        return wishlist_lists.get(id);
    }

    /**
     * Gets all lists that belongs to a certain user.
     * 
     * @param uid
     *            the users uid.
     * @return a list of wish lists.
     */
    public List<WishList> getMyWishList(String uid) {
        List<WishList> lists = new ArrayList<WishList>();
        for (WishList list : wishlist_lists.values()) {
            if (list.getOwner().getUid().equals(uid)) {
                lists.add(list);
            }
        }
        return lists;
    }

    /**
     * Adds a wish list to the map of wish lists.
     * 
     * @param wishlist
     *            a wishlist object.
     */
    public void addWishList(WishList wishlist) {
        wishlist_lists.put(wishlist.getId(), wishlist);
    }

    /**
     * Deletes a wish list.
     * 
     * @param listId
     *            the list id to delete.
     */
    public void deleteWishList(String listId) {
        wishlist_lists.remove(listId);
    }

    /**
     * Adds a product to the map of products.
     * 
     * @param product
     *            a product object.
     */
    public void addProduct(String wishListid, Product product) {
        WishList wishList = wishlist_lists.get(wishListid);
        wishList.addProduct(product);
    }

    public void deleteProduct(String listId, String productId) {
        WishList list = WishListService.getInstance().getWishList(listId);
        list.deleteProduct(productId);
    }

    /**
     * 
     * @author Emil Stjerneman
     * 
     */
    class ValueComparator implements Comparator<WishList> {

        Map<String, WishList> base;

        public ValueComparator(Map<String, WishList> base) {
            this.base = base;
        }

        @Override
        public int compare(WishList wl1, WishList wl2) {
            if (base.get(wl1).getTimestamp() >= base.get(wl2).getTimestamp()) {
                return -1;
            }
            else {
                return 1;
            }
        }
    }
}
