<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="wishListTag" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>${headTitle} - Wishlister</title>
	<%@ include file="styles.jsp" %>
	<%@ include file="scripts.jsp" %>
</head>
<body>
	<%@ include file="header.jsp" %>
	
	<div id="container">
	
		<wishListTag:error-tag errors="${errors}" />
		
		<section id="product-form" class="clearfix">
			<h2 class="pane-title">${pageTitle}</h2>
			
			<form action="<c:url value="${formAction}" />" method="post" id="form-add-product" class="col-40 hide-labels">
				<div class="form-item">
					<label for="field-add-product-name">Name</label>
					<input type="text" id="field-add-product-name" name="name" value="${product.name}" placeholder="What is the product name?" />
				</div>
				
				<div class="form-item">
					<label for="field-add-product-price">Price</label>
					<input type="text" id="field-add-product-price" name="price" value="${product.price}" placeholder="Does it have a price?" />
				</div>
	
				<div class="form-item">
					<label for="field-add-product-url">URL</label>
					<input type="text" id="field-add-product-url" name="url" value="${product.url}" placeholder="An URL to it would be nice" />
				</div>
			
				<div class="form-actions">
					<input type="submit" id="field-add-product-submit" name="op" value="${buttonValue}" />
				</div>
				
				<c:if test="${not empty listId}">
					<input type="hidden" name="listId" value="${listId}" />
				</c:if>
				<c:if test="${not empty productId}">
					<input type="hidden" name="productId" value="${productId}" />
				</c:if>
			</form>
		</section>
		<a href="<c:url value="/wishlist?listId=${listId}" />" class="backlink">Back to wish list.</a>
	</div>
	
	<%@ include file="footer.jsp" %>
</body>
</html>