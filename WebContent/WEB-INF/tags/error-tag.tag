<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="errors" required="true" type="java.util.List" %>

<c:if test="${not empty errors}">
	<section class="error">
		<ul>
			<c:forEach items="${errors}" var="error">
				<li>${error}</li>
			</c:forEach>
		</ul>
	</section>
</c:if>