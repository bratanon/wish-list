package se.stjerneman.wishlists.domain;

import java.util.UUID;

/**
 * Manage products.
 * 
 * @author Emil Stjerneman
 * 
 */
public class Product {

    /**
     * This products universally unique identifier.
     */
    private String id;

    /**
     * This products name.
     */
    private String name;

    /**
     * This products price. A price is not required.
     */
    private float price;

    /**
     * This products URL. A URL is not required.
     */
    private String url;

    /**
     * This products owner.
     */
    private User owner;

    public Product(String name, User owner) {
        setId();
        setName(name);
        setOwner(owner);
    }

    public String getId() {
        return id;
    }

    private void setId() {
        id = UUID.randomUUID().toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public User getOwner() {
        return owner;
    }

    private void setOwner(User owner) {
        this.owner = owner;
    }
}
